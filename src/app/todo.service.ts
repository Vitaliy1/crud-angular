import { Injectable } from '@angular/core';
import { Todo } from './todo';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/index";

@Injectable()
export class TodoService
{
    private apiUrl = 'http://crud-yii2-api/posts';  // URL to web api

    private todos;

    constructor(private http: HttpClient) {}

    /*
     * Имитируем метод GET при обращении к /todos
     * */
    getAllTodos(): Observable<Todo[]>
    {
        console.log('getAllTodos service');
        return this.http.get<Todo[]>(this.apiUrl);
    }

    /*
    * Имитируем метод POST при обращении к /todos
    * */
    addTodo(values): Observable<Todo> {
        console.log('addTodo service: ', values);
        return this.http.post<Todo>(this.apiUrl + '/create', values);
    }

    // /*
    //  * Имитируем метод DELETE при обращении к /todos/:id
    //  * */
    deleteTodoById( id: number ): Observable<{}>
    {
        console.log('deleteTodoById service: ', id);
        return this.http.delete(this.apiUrl + '/delete/' + id);
    }

    //
    // /*
    //  * Имитируем метод PUT при обращении к /todos/:id
    //  * */
    updateTodoById( id: number, values: Object = {} ): Observable<Todo>
    {
        console.log('updateTodoById service: ', values);
        return this.http.put<Todo>(this.apiUrl + '/update/' + id, values);
    }

    //
    // /*
    //  * Имитируем метод GET при обращении к /todos/:id
    //  * */
    getTodoById( id: number ): any
    {
        return this.http.get<Todo>(this.apiUrl + '/' + id);
    }

    // /*
    //  * Изменить статус записи
    //  * */
    toggleTodoComplete( todo: Todo )
    {
        console.log('toggleTodoComplete service: ', todo);
        return this.updateTodoById( todo.id, {
            complete: !todo.complete
        } );
    }
}