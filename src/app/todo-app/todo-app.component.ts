import { Component, OnInit } from '@angular/core';
import {Todo} from "../todo";
import { TodoService } from "../todo.service";

@Component({
      selector: 'app-todo-app',
      templateUrl: './todo-app.component.html',
      styleUrls: ['./todo-app.component.css']
})
export class TodoAppComponent implements OnInit {
    newTodo: Todo = new Todo();
    public todos: Todo[] = [];

    constructor(private todoService: TodoService) { }

    toggleTodoComplete(todo) {
        this.todoService
            .toggleTodoComplete(todo)
            .subscribe(todo_ => {console.log('toggleTodoComplete component: ', todo_); todo = todo_;});
    }

    addTodo() {
        this.todoService
            .addTodo(this.newTodo)
            .subscribe(todo => {console.log('addTodo component: ', todo); this.todos.push(todo)});
    }

    removeTodo(todo) {
        this.todoService
            .deleteTodoById(todo.id)
            .subscribe(() => {console.log('removeTodo component: ', todo.id); this.todos = this.todos.filter(item => item != todo)});
    }

    ngOnInit() {
        this.todoService.getAllTodos()
            .subscribe(res => {console.log('getAllTodos component'); this.todos = res});
    }

}
